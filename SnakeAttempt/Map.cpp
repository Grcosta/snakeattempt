#include "Map.h"
#include "Shared.h"

SnakeGame::Map::Map(unsigned mapWidth, unsigned mapHeight) :
	mapWidth(mapWidth),
	mapHeight(mapHeight),
	lastFoodX(0),
	lastFoodY(0),
	mapData(mapHeight, std::vector<int>(mapWidth, 0))
{
}

void SnakeGame::Map::initalizeMapData()
{
	for (unsigned i = 0; i < mapWidth; i++)
	{
		//all columns of the first row is wall.
		mapData[0][i] = Shared::MapTile::wall;
		//all columns of the last row is wall.
		mapData[mapHeight - 1][i] = Shared::MapTile::wall;
	}

	for (unsigned i = 0; i < mapHeight; i++)
	{
		//first column of each row is wall.
		mapData[i][0] = Shared::MapTile::wall;
		//last column of each row is wall.
		mapData[i][mapWidth - 1] = Shared::MapTile::wall;
	}
}

void SnakeGame::Map::generateFood()
{
	unsigned x, y;
	do
	{
		x = rand() % (mapWidth - 2) + 1;
		y = rand() % (mapHeight - 2) + 1;
	} while (mapData[y][x] != Shared::MapTile::walkable);

	mapData[y][x] = Shared::MapTile::food;
	lastFoodX = x;
	lastFoodY = y;
}

void SnakeGame::Map::clearFood()
{
	mapData[lastFoodY][lastFoodX] = Shared::MapTile::walkable;
}

int SnakeGame::Map::getMapValue(unsigned x, unsigned y)
{
	if (x >= mapWidth) x = mapWidth - 1;
	if (y >= mapHeight) y = mapHeight - 1;

	return mapData[y][x];
}

void SnakeGame::Map::setMapValue(unsigned x, unsigned y, int val)
{
	mapData[y][x] = val;
}

const std::vector<std::vector<int>>& SnakeGame::Map::getMapData() const
{
	return mapData;
}

void SnakeGame::Map::clearSnakeTiles()
{
	for (unsigned y = 0; y < mapHeight; y++)
	{
		for (unsigned x = 0; x < mapWidth; x++)
		{
			if (mapData[y][x] > Shared::MapTile::walkable)
			{
				mapData[y][x]--;
			}
		}
	}
}