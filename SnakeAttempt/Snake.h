#ifndef SNAKE_H
#define SNAKE_H

namespace SnakeGame
{
	struct Snake
	{
		unsigned headX;
		unsigned headY;
		unsigned bodyLength;
		int currentDirection;

		void move(int dX, int dY)
		{
			headX += dX;
			headY += dY;
		}
	};
}

#endif