#ifndef RENDERER_H
#define RENDERER_H
#include <vector>
#include <memory>

namespace SnakeGame
{
	namespace Core
	{
		class Renderer
		{
		public:
			Renderer();
			virtual ~Renderer();
			virtual void setMapData(const std::vector<std::vector<int>>& mapData) = 0;
			virtual void render(unsigned sceneWidth, unsigned sceneHeight) = 0;
			virtual void clearScreen() = 0;
		protected:
			std::unique_ptr<const std::vector<std::vector<int>>> uptrMapData;
		};
	}
}

#endif