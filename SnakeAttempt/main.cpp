#include "GameEngine.h"
#include <iostream>

int main()
{

	//Create renderer
	SnakeGame::Core::Win32ConsoleRenderer renderer;
	//Create game engine and pass renderer to it.
	SnakeGame::GameEngine snakeGame(&renderer);
	//run the game.
	snakeGame.run();

	std::cout << "== Game over! ==" << std::endl;
	std::cout << "Your score: " << snakeGame.getScore() << std::endl;
	std::cin.ignore();

	return 0;
}