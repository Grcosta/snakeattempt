#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include "Map.h"
#include "Snake.h"
#include <chrono>
#include "Win32ConsoleRenderer.h"

namespace SnakeGame
{
	class GameEngine
	{
		typedef std::chrono::milliseconds ms;
		typedef std::chrono::high_resolution_clock clock;

	public:
		GameEngine(Core::Renderer* const pRenderer);
		~GameEngine();

		//Methods
		void run();

		//Accessors
		unsigned getScore() const { return myScore; }

	private:
		bool isRunning;
		int myScore;
		int m_msPerFrame;
		Map myMap;
		Snake mSnake;
		std::unique_ptr<Core::Renderer> p_renderer;

		//Methods
		void processInput();
		void update();
		void draw() const;
	};
}

#endif