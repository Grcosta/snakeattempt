#include "Renderer.h"

SnakeGame::Core::Renderer::Renderer() : uptrMapData(nullptr)
{

}

SnakeGame::Core::Renderer::~Renderer()
{
	if (uptrMapData != nullptr)
	{
		uptrMapData.release();
	}
}