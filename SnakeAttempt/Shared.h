#ifndef SHARED_H
#define SHARED_H

namespace SnakeGame
{
	namespace Shared
	{
		constexpr unsigned MAP_WIDTH = 40;
		constexpr unsigned MAP_HEIGHT = 30;
		constexpr unsigned FPS = 5;

		enum MapTile
		{
			wall = -2,
			food,
			walkable,
			snakeBody
		};

		enum Direction
		{
			up,
			right,
			down,
			left
		};
	}
}

#endif