#ifndef MAP_H
#define MAP_H

#include <vector>

namespace SnakeGame
{
	class Map
	{
	public:
		Map(unsigned mapWidth, unsigned mapHeight);
		void initalizeMapData();
		void generateFood();
		void clearFood();

		//Methods
		int getMapValue(unsigned x, unsigned y);
		void setMapValue(unsigned x, unsigned y, int val);
		void clearSnakeTiles();

		//Accessors
		const std::vector<std::vector<int>>& getMapData() const;

	private:
		unsigned mapWidth, mapHeight;
		unsigned lastFoodX, lastFoodY;
		std::vector<std::vector<int>> mapData;
	};
}

#endif