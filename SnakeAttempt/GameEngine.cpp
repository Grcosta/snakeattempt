#include <conio.h>
#include <thread>
#include "GameEngine.h"
#include "Shared.h"

SnakeGame::GameEngine::GameEngine(Core::Renderer* const pRenderer) :
	isRunning(false),
	myScore(0),
	m_msPerFrame(1000 / Shared::FPS),
	myMap(Shared::MAP_WIDTH, Shared::MAP_HEIGHT),
	mSnake{ Shared::MAP_WIDTH / 2, Shared::MAP_HEIGHT / 2, 3, 0 },
	p_renderer(pRenderer)
{
	if (p_renderer != nullptr)
	{
		p_renderer->setMapData(myMap.getMapData());
	}
}

SnakeGame::GameEngine::~GameEngine()
{
	if (p_renderer != nullptr)
	{
		p_renderer.release();
	}
}

void SnakeGame::GameEngine::run()
{
	//initialize map walls
	myMap.initalizeMapData();
	//place snake object at it's initialized position (center of the map)
	myMap.setMapValue(mSnake.headX, mSnake.headY, Shared::MapTile::snakeBody);
	//generate first food on the map.
	myMap.generateFood();
	//set game state to running
	isRunning = true;

	//main game loop
	while (isRunning)
	{
		auto start = clock::now();

		//process user input
		processInput();

		//update game objects & conditions
		update();

		//draw (render) the scene.
		draw();

		auto sleep = std::chrono::duration_cast<ms>(start + ms(m_msPerFrame) - clock::now());
		std::this_thread::sleep_for(sleep);
		
	}
}

void SnakeGame::GameEngine::processInput()
{
	//check if there is a keyboard interrupt
	if (_kbhit())
	{
		auto key = static_cast<char>(_getch());
		switch (key)
		{
			case 'w':
				if (mSnake.currentDirection != Shared::Direction::down)
				{
					mSnake.currentDirection = Shared::Direction::up;
				}
				break;
			case 's':
				if (mSnake.currentDirection != Shared::Direction::up)
				{
					mSnake.currentDirection = Shared::Direction::down;
				}
				break;
			case 'a':
				if (mSnake.currentDirection != Shared::Direction::right)
				{
					mSnake.currentDirection = Shared::Direction::left;
				}
				break;
			case 'd':
				if (mSnake.currentDirection != Shared::Direction::left)
				{
					mSnake.currentDirection = Shared::Direction::right;
				}
				break;
			default:;
		}
	}
}


void SnakeGame::GameEngine::update()
{
	//update snake position
	switch (mSnake.currentDirection)
	{
		case Shared::Direction::up:
			mSnake.move(0, -1);
			break;
		case Shared::Direction::right:
			mSnake.move(1, 0);
			break;
		case Shared::Direction::left:
			mSnake.move(-1, 0);
			break;
		case Shared::Direction::down:
			mSnake.move(0, 1);
			break;
		default:;
	}

	int currentMapValue = myMap.getMapValue(mSnake.headX, mSnake.headY);
	//check if we hit a food
	if (currentMapValue == Shared::MapTile::food)
	{
		//increase snake body length
		mSnake.bodyLength++;
		//clear current food
		myMap.clearFood();
		//generate new food.
		myMap.generateFood();
		//update score
		myScore += 10;
		//Speed up the game
		m_msPerFrame -= 10;
	}
	else if (currentMapValue != Shared::MapTile::walkable)
	{
		isRunning = false;
	}

	myMap.clearSnakeTiles();
	myMap.setMapValue(mSnake.headX, mSnake.headY, mSnake.bodyLength);
}

void SnakeGame::GameEngine::draw() const
{
	if (isRunning)
	{
		p_renderer->render(Shared::MAP_WIDTH, Shared::MAP_HEIGHT);
	}
	else
	{
		p_renderer->clearScreen();
	}
}