#include "Win32ConsoleRenderer.h"
#include <iostream>

HANDLE SnakeGame::Core::Win32ConsoleRenderer::m_outputHandle = nullptr;

SnakeGame::Core::Win32ConsoleRenderer::Win32ConsoleRenderer() :
	m_buffered(false),
	mapDataCache({})
{
	m_outputHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	Win32ConsoleRenderer::clearScreen();
	setWindowSize();
	hideCursor();
}

SnakeGame::Core::Win32ConsoleRenderer::~Win32ConsoleRenderer()
{
	if (this->uptrMapData != nullptr)
	{
		this->uptrMapData.release();
	}
}

void SnakeGame::Core::Win32ConsoleRenderer::setMapData(const std::vector<std::vector<int>>& mapData)
{
	uptrMapData.reset(&mapData);
	mapDataCache.resize(uptrMapData->size());
	initializeBuffer();
}

void SnakeGame::Core::Win32ConsoleRenderer::render(unsigned sceneWidth, unsigned sceneHeight)
{
	for (unsigned y = 0; y < sceneHeight; y++)
	{
		for (unsigned x = 0; x < sceneWidth; x++)
		{
			auto currentMapValue = this->uptrMapData->at(y)[x];
			auto cachedMapValue = mapDataCache[y][x];
			if (currentMapValue == cachedMapValue) continue;

			this->mapDataCache[y][x] = currentMapValue;
			setCursorPosition(x, y);
			std::cout << mapValueToChar(static_cast<Shared::MapTile>(currentMapValue));
		}
	}

	std::cout.flush();
}

void SnakeGame::Core::Win32ConsoleRenderer::setWindowSize()
{
	HWND windowHandle = GetConsoleWindow();
	RECT r;

	GetWindowRect(windowHandle, &r);
	MoveWindow(windowHandle, r.left, r.top, Shared::MAP_WIDTH * 10, Shared::MAP_HEIGHT * 20, TRUE);
}

void SnakeGame::Core::Win32ConsoleRenderer::clearScreen()
{
	CONSOLE_SCREEN_BUFFER_INFO bufferInfo;
	COORD topLeft{ 0,0 };

	std::cout.flush();

	if (!GetConsoleScreenBufferInfo(m_outputHandle, &bufferInfo))
	{
		std::cout << "BUFFER ERROR" << std::endl;
	}

	DWORD length = bufferInfo.dwSize.X * bufferInfo.dwSize.Y;
	DWORD written;

	FillConsoleOutputCharacter(m_outputHandle, TEXT(' '), length, topLeft, &written);
	FillConsoleOutputAttribute(m_outputHandle, bufferInfo.wAttributes, length, topLeft, &written);

	SetConsoleCursorPosition(m_outputHandle, topLeft);
}

void SnakeGame::Core::Win32ConsoleRenderer::setCursorPosition(unsigned x, unsigned y)
{
	COORD coord{ static_cast<short>(x), static_cast<short>(y) };
	SetConsoleCursorPosition(m_outputHandle, coord);
}

void SnakeGame::Core::Win32ConsoleRenderer::hideCursor()
{
	CONSOLE_CURSOR_INFO cursorInfo{ 100,FALSE };
	SetConsoleCursorInfo(m_outputHandle, &cursorInfo);
}

char SnakeGame::Core::Win32ConsoleRenderer::mapValueToChar(Shared::MapTile mapValue)
{
	switch (mapValue)
	{
		case Shared::MapTile::wall:
			return '=';
		case Shared::MapTile::food:
			return '@';
		case Shared::MapTile::snakeBody:
			return 'o';
		case Shared::MapTile::walkable:
			return ' ';
		default:
			return 'o';
	}
}

void SnakeGame::Core::Win32ConsoleRenderer::initializeBuffer()
{
	if (!m_buffered)
	{
		for (unsigned int y = 0; y < mapDataCache.size(); y++)
		{
			auto colSize = this->uptrMapData->at(y).size();
			mapDataCache[y].resize(colSize);
		}

		m_buffered = true;
	}
}