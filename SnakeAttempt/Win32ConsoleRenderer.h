#ifndef WIN32_CONSOLE_RENDERER_H
#define WIN32_CONSOLE_RENDERER_H
#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <vector>
#include <Windows.h>
#include "Shared.h"
#include "Renderer.h"

namespace SnakeGame
{
	namespace Core
	{
		class Win32ConsoleRenderer : public Renderer
		{
		public:
			Win32ConsoleRenderer();
			~Win32ConsoleRenderer();
			void setMapData(const std::vector<std::vector<int>>& mapData) override;
			void render(unsigned sceneWidth, unsigned sceneHeight) override;
			void clearScreen() override;
		private:
			bool m_buffered;
			std::vector<std::vector<int>> mapDataCache;
			static HANDLE m_outputHandle;
			static void setWindowSize();
			static void setCursorPosition(unsigned x, unsigned y);
			static void hideCursor();
			static char mapValueToChar(Shared::MapTile mapValue);
			void initializeBuffer();
		};
	}
}

#endif